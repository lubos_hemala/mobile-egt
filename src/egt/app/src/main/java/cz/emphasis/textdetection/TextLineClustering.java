package cz.emphasis.textdetection;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static cz.emphasis.textdetection.TextDetection.TEXTLINE_OPPOSITE_PRECISION_LOW;

public class TextLineClustering {

    public Set<TextLine> Cluster(Set<Region> regions){
        Map<Region, Set<TextLine>> intersections = FindIntersections(regions);
        Set<TextLine> lines = Cluster(intersections);
        return lines;
    }

    public Set<TextLine> Cluster(Map<Region, Set<TextLine>> intersections){
        Set<TextLine> lines = new HashSet<>();

        // Prepare clustering of adjacent lines
        Map<TextLine, Set<TextCluster>> clustersByLine = new HashMap<>();
        TreeSet<TextCluster> clusters = new TreeSet<>();
        for (Map.Entry<Region, Set<TextLine>> entry : intersections.entrySet()){
            Region r1 = entry.getKey();

            // Collect possible combinations
            for (TextLine l1 : entry.getValue()){
                for (TextLine l2 : entry.getValue()){
                    if (l1.equals(l2))
                        continue;

                    TextCluster cluster = new TextCluster(l1, l2, r1);
                    if (cluster.canConnect()) {
                        clusters.add(cluster);
                        clustersByLine.computeIfAbsent(l1, x -> new HashSet<>()).add(cluster);
                        clustersByLine.computeIfAbsent(l2, x -> new HashSet<>()).add(cluster);
                    }
                }
            }
        }

        int i = 0;
        Set<Region> merged = new HashSet<>();
        while (!clusters.isEmpty()){
            TextCluster cluster = clusters.pollFirst();
            if (merged.contains(cluster.root))
                continue;

            DirectedTextLine d1 = cluster.getFirst();
            DirectedTextLine d2 = cluster.getSecond();
            Region r1 = d1.getLast();
            Region r2 = d2.getLast();
            Set<TextLine> i1 = intersections.get(r1);
            Set<TextLine> i2 = intersections.get(r2);

            // Also check that there isn't a line that contains either cluster end and which is not opposite
            // to the current cluster and is closely packed at the intersection
            boolean isSuperseded = lines.stream()
                .anyMatch(line -> {
                    Region r;
                    DirectedTextLine d;
                    if (line.regions.contains(r1)) {
                        r = r1;
                        d = d1;
                    } else if (line.regions.contains(r2)) {
                        r = r2;
                        d = d2;
                    } else return false;

                    // Exempt opposite lines, otherwise clusters next to each other will not get merged
                    if (d.isOpposite(line, TEXTLINE_OPPOSITE_PRECISION_LOW))
                        return false;

                    int inx = line.line.indexOf(r);
                    double dist1 = line.distanceAt(inx);
                    double dist2 = d.getLast().centroidDistance(d.getSecondLast());
                    return dist1 > dist2;
                });
            if (isSuperseded)
                continue;

            if (++i == 2988) {
                System.err.println();
                //break;
            }

            TextLine l3 = cluster.connect();
            lines.add(l3);

            // Collect lines to remove to be able to remove their clusters as well
            List<TextLine> removeLines = new ArrayList<>(cluster.lines());

            // Remove lines that intersect at the root of the merged cluster because they are further from the root
            Set<TextLine> i0 = intersections.get(cluster.root);
            removeLines.addAll(i0);

            // Remove lines starting at root from their other intersections
            i0.forEach(line -> {
                Region r = line.getFirst().equals(cluster.root) ? line.getLast() : line.getFirst();
                Set<TextLine> ix = intersections.get(r);
                ix.remove(line);
            });
            // Remove lines starting at root from their root intersections
            i0.clear();

            // Remove clusters containing lines that were merged or discarded
            removeLines.forEach(line ->{
                Set<TextCluster> prevClusters = clustersByLine.get(line);
                if (prevClusters != null) {
                    clusters.removeAll(prevClusters);
                    prevClusters.clear();
                }
            });

            // Remove other clusters with roots at the ends of the previous cluster which are not opposite
            // This prevents alternative vertical lines at the beginning and end of horizontal lines

            /*
            for (Pair<DirectedTextLine, Set<TextLine>> guide : Arrays.asList(Pair.create(d1, i1), Pair.create(d2, i2))){
                DirectedTextLine dl = guide.getFirst();
                Set<TextLine> intersection = guide.getSecond();
                intersection.forEach(line -> {
                    Set<TextCluster> prevClusters = clustersByLine.get(line);
                    if (prevClusters == null)
                        return;

                    List<TextCluster> removeClusters = prevClusters.stream()
                        .filter(cls -> !cls.directedLines().stream().anyMatch(d -> d.isOpposite(dl, TEXTLINE_OPPOSITE_PRECISION)))
                        .collect(Collectors.toList());
                    prevClusters.removeAll(removeClusters);
                    clusters.removeAll(removeClusters);
                });
            }
            */

            lines.removeAll(removeLines);

            // Reconnect the new line into clusters
            i1.remove(d1.line);
            List<TextCluster> c1 = i1.stream()
                .map(line -> TextCluster.create(line, l3, r1))
                //.filter(c -> c.isOpposite())
                .collect(Collectors.toList());
            c1.removeIf(c -> !c.canConnect());
            clusters.addAll(c1);

            i2.remove(d2.line);
            List<TextCluster> c2 = i2.stream()
                .map(line -> TextCluster.create(line, l3, r2))
                //.filter(c -> c.isOpposite())
                .collect(Collectors.toList());
            c2.removeIf(c -> !c.canConnect());
            clusters.addAll(c2);

            // Store each new cluster for easy retrieval by both of its lines
            Stream.concat(c1.stream(), c2.stream()).forEach(c ->
                c.lines().forEach(line ->
                    clustersByLine.computeIfAbsent(line, x -> new HashSet<>()).add(c)));

            // Allow the new line to be connected
            i1.add(l3);
            i2.add(l3);

            merged.add(cluster.root);
        }

        //lines.removeIf(l -> l.size() <= 3);
        return lines;
    }

    public Map<Region, Set<TextLine>> FindIntersections(Set<Region> regions){
        // Prepare intersections for each region
        Map<Region, Set<TextLine>> intersections = regions.stream()
            .collect(Collectors.toMap(Function.identity(), r -> new HashSet<>()));

        // Find letter candidates which can be paired together based on proximity and stroke, size and color similarity
        for (Region r1 : regions){
            for (Region r2 : regions){
                if (r1 == r2)
                    continue;

                // Letters having similar stroke width
                double m1 = r1.swtMedian, m2 = r2.swtMedian;
                double swtMedianRatio = m1 > m2 ? m1/m2 : m2/m1;
                if (swtMedianRatio > 2)
                    continue;

                // Letters having similar height
                double h1 = r1.height, h2 = r2.height;
                double heightRatio = h1 > h2 ? h1/h2 : h2/h1;
                if (heightRatio > 2)
                    continue;

                // Letters at a centroidDistance at most 3 times the width of the wider one
                double w1 = r1.width, w2 = r2.width;
                double radius = (w2 > w1 ? w2 : w1) * 3;
                double dist = r1.centroidDistance(r2);
                if (dist > radius)
                    continue;

                TextLine line = new TextLine(r1, r2);
                intersections.get(r1).add(line);
                intersections.get(r2).add(line);
            }
        }

        return intersections;
    }
}
