package cz.emphasis.textdetection.helpers;

import org.opencv.core.Mat;

public class MatByteIndexer extends MatIndexer {
    private final byte[] buffer;

    public MatByteIndexer(Mat source){
        super(source);
        this.buffer = new byte[size];
        source.get(0, 0, buffer);
    }

    public int get(int x, int y){
        byte value = buffer[at(x,y)];
        int result = Byte.toUnsignedInt(value);
        return result;
    }

    public int get(DiscretePoint p){
        return get(p.x, p.y);
    }

    public void put (int x, int y, short value){
        byte result = (byte)(value & 0xFF);
        buffer[at(x,y)] = result;
    }

    public void put(DiscretePoint p, short value){
        put(p.x, p.y, value);
    }

    @Override
    public void close() throws Exception {
        source.put(0, 0, buffer);
    }
}
