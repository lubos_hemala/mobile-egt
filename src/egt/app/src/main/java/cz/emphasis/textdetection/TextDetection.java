package cz.emphasis.textdetection;

import cz.emphasis.textdetection.helpers.*;
import org.opencv.core.*;
import org.opencv.imgproc.Imgproc;

import java.util.*;
import java.util.stream.Collectors;

import static java.lang.Math.*;

public class TextDetection {

    public static final double PI2 = PI*2;
    private final double CANNY_T1 = 60; // This should be rather lower or it will skip some edges
    private final double CANNY_T2 = CANNY_T1 * 2.5;
    public final double SWT_RAYS_OPPOSITE_PRECISION = PI * 0.5;
    private final double SWT_COMPONENT_RATIO = 3.0;
    public static final int SWT_MAX_VALUE = Integer.MAX_VALUE;
    private final int LABEL_MAX_VALUE = Integer.MAX_VALUE;
    public static final double TEXTLINE_OPPOSITE_PRECISION_LOW = PI * 0.12;
    public static final double TEXTLINE_OPPOSITE_PRECISION_HIGH = PI * 0.22;

    Size size;
    int width;
    int height;

    public Mat gray = new Mat();
    public Mat blur = new Mat();
    public Mat gradx = new Mat();
    public Mat grady = new Mat();
    public Mat edges = new Mat();

    public final List<BresenhamLine> rays = new ArrayList<>(width*height/3);
    public final Set<Region> regions = new HashSet<>(1000);
    public final Set<TextLine> lines = new HashSet<>(3000);

    public ArrayDouble2D swt;
    public ArrayInteger2D labels;

    private MatByteIndexer edgesInx;
    private MatFloatIndexer gradxInx;
    private MatFloatIndexer gradyInx;

    public void findText(Mat image, double scale){
        size = image.size();
        if (scale != 1) {
            width = (int) ceil(size.width * scale);
            height = (int) ceil(size.height * scale);
            size = new Size(width, height);
        } else {
            width = (int)size.width;
            height = (int)size.height;
        }

        rays.clear();
        regions.clear();
        lines.clear();

        if (!size.equals(gray.size())){
            gray = new Mat(size, CvType.CV_32S);
            blur = new Mat(size, CvType.CV_32F);
            gradx = new Mat(size, CvType.CV_32F);
            grady = new Mat(size, CvType.CV_32F);
            edges = new Mat(size, CvType.CV_8U);
            //swt = new Mat(size, CvType.CV_32F, Scalar.all(SWT_MAX_VALUE));
            //labels = new Mat(size, CvType.CV_16U, Scalar.all(LABEL_MAX_VALUE));
            //lined = new Mat(size, CvType.CV_8U, Scalar.all(0));
            swt = new ArrayDouble2D(width, height);
            labels = new ArrayInteger2D(width, height);
        } else {
            //swt.setTo(Scalar.all(SWT_MAX_VALUE));
            //labels.setTo(Scalar.all(LABEL_MAX_VALUE));
            //lined.setTo(Scalar.all(0));
        }

        if (scale != 1) {
            Imgproc.resize(image, gray, size, 0,0, Imgproc.INTER_LINEAR);

            // Unsharp masking
            //Imgproc.GaussianBlur(gray, blur, new Size(0,0), 3);
            //Core.addWeighted(gray, 1.5, blur, -0.5, 0, gray);

            image = gray;
        }

        swt.fill(SWT_MAX_VALUE);
        labels.fill(SWT_MAX_VALUE);

        // Convert to gray-scale
        Imgproc.cvtColor(image, gray, Imgproc.COLOR_RGB2GRAY);

        // Compute the dx and dy gradient
        prepareGradient();
        gradxInx = new MatFloatIndexer(gradx);
        gradyInx = new MatFloatIndexer(grady);

        // Detect edges using Canny edge detector
        Imgproc.Canny(gray, edges, CANNY_T1, CANNY_T2, 3, false);
        edgesInx = new MatByteIndexer(edges);

        // Stroke width transform
        findStrokeRays(true);

        markStrokeWidth();
        labelStrokes();
        findLetterCandidates();
        findTextLines();
    }

    private void prepareGradient(){
        gray.convertTo(blur, CvType.CV_32F, 1.0/255.0);

        Imgproc.GaussianBlur(blur, blur, new Size(5, 5), 0);

        Imgproc.Scharr(blur, gradx, -1, 1, 0);
        Imgproc.Scharr(blur, grady, -1, 0, 1);

        Imgproc.GaussianBlur(gradx, gradx, new Size(3, 3), 0);
        Imgproc.GaussianBlur(grady, grady, new Size(3, 3), 0);
    }

    public double gradientDirection(DiscretePoint p, boolean darkOnLight){
        float dx = gradxInx.get(p);
        float dy = gradyInx.get(p);
        double direction = Helpers.direction(-dy, dx, darkOnLight);

        //p.put(phase, centroidDirection * 20 + 255/4);

        return direction;
    }

    private void findStrokeRays(boolean darkOnLight){
        DiscretePoint p = new DiscretePoint();

        int c = 0;
        for (int y = 0; y < size.height; y++){
            for (int x = 0; x < size.width; x++){
                p.moveTo(x,y);
                if (!isEdge(p))
                    continue;

                double theta = gradientDirection(p, darkOnLight);
                // Shoot multiple rays in directions closely opposite to the gradient
                double[] directions = new double[]{
                    theta - PI * 0.15,
                    theta,
                    theta + PI * 0.15,
                };

                for(double direction : directions) {
                    // Follow a line in the centroidDirection opposite to the gradient until an edge is encountered
                    BresenhamLine line = new BresenhamLine(p, direction, width, height, true, false);
                    int dx = (int)signum(cos(direction));
                    int dy = (int)signum(-sin(direction));
                    int dist = 1;
                    for (DiscretePoint r : line){
                        if (!r.isInside(width, height))
                            break;

                        //if (dist % 2 == 0 && dist > 1) r.put(phase, 255/4);

                        dist++;
                        double alpha;
                        if (isEdge(r)) {
                            alpha = gradientDirection(r, darkOnLight);
                        } else {
                            // Some lined escape due to 8-way line crossing (no intersection point)
                            // More proper solution would be to rewrite BresenhamLine to be 8-way contiguous
                            DiscretePoint r1 = r.shiftBy(-dx,0);
                            DiscretePoint r2 = r.shiftBy(0, -dy);
                            // This filters most of these lined where edge points are correctly identified
                            if (dist <= 2 || !r1.isInside(width, height) || !r2.isInside(width, height) || !isEdge(r1) || !isEdge(r2))
                                continue;

                            alpha = gradientDirection(r, darkOnLight);
                        }

                        // Check if the other edge has roughly an opposite gradient (a stroke)
                        if (Helpers.isOpposite(alpha, theta, SWT_RAYS_OPPOSITE_PRECISION)) {
                            BresenhamLine ray = new BresenhamLine(p, r);
                            rays.add(ray);
                        }
                        break;
                    }
                }

                c++;
            }
        }
    }

    private boolean isEdge(DiscretePoint p){
        return edgesInx.get(p) > 0;
    }

    private void markStrokeWidth(){
        for (BresenhamLine line : rays) {
            double length = line.length();
            for (DiscretePoint r : line) {
                double current = swt.get(r);
                if (length < current)
                    swt.put(r, length);
            }
        }
        for (BresenhamLine line : rays) {
            List<Double> values = line.points().map(x -> swt.get(x)).collect(Collectors.toList());
            double median = Helpers.median(values);
            for (DiscretePoint r : line) {
                double current = swt.get(r);
                if (median < current)
                    swt.put(r, median);
            }
        }
    }

    private static final int[][] neighbourhood8 = new int[][]{
        { -1, -1 }, { +0, -1 }, { +1, -1 },
        { -1, +0 }, { +1, +0 },
        { -1, +1 }, { +0, +1 }, { +1, +1 },
    };

    private boolean isComponent(DiscretePoint p){
        return labels.get(p) < LABEL_MAX_VALUE;
    }

    private boolean isStroke(DiscretePoint p){
        return isStroke(swt.get(p));
    }

    private boolean isStroke(double sw){
        return sw < SWT_MAX_VALUE;
    }

    private boolean isSameStroke(double sw1, double sw2){
        double ratio = sw1 > sw2 ? sw1/sw2 : sw2/sw1;
        return ratio < SWT_COMPONENT_RATIO;
    }

    private void labelStrokes(){
        int label = 0;
        TreeSet<DiscretePoint> work = new TreeSet<>(DiscretePoint.comparatorDyDx);
        TreeSet<DiscretePoint> resolved = new TreeSet<>(DiscretePoint.comparatorDyDx);
        TreeSet<DiscretePoint> colored = new TreeSet<>(DiscretePoint.comparatorDyDx);

        // Use flood fill to label components
        DiscretePoint p = new DiscretePoint();
        for (int y = 0; y < height; y++){
            for (int x = 0; x < width; x++){
                p.moveTo(x, y);
                if (isComponent(p) || !isStroke(p))
                    continue;

                // Mark this point as a root of a new region
                label++;
                labels.put(p, label);
                work.add(new DiscretePoint(p.x, p.y));

                while (!work.isEmpty()){
                    DiscretePoint current = work.first();
                    work.remove(current);

                    if (resolved.contains(current))
                        continue;

                    resolved.add(current);
                    double sw1 = swt.get(current);
                    for (int[] shift : neighbourhood8){
                        DiscretePoint next = current.shiftBy(shift[0], shift[1]);
                        if (!next.isInside(width, height))
                            continue;

                        if (resolved.contains(next) || work.contains(next))
                            continue;

                        double sw2 = swt.get(next);
                        if (isStroke(sw2)){
                            if (isSameStroke(sw1, sw2)){
                                labels.put(next, label);
                                work.add(next);
                            }
                            continue;
                        }

                    }
                }

                List<Double> sw = resolved.stream()
                    .map(swt::get)
                    .filter(this::isStroke)
                    .collect(Collectors.toList());
                TreeSet<DiscretePoint> points = (TreeSet<DiscretePoint>)resolved.clone();
                regions.add(Region.create(label, points, sw));

                resolved.clear();
                colored.clear();
            }
        }
    }

    double regionSizeAverage;
    double regionSizeVariance;
    double regionSizeDeviation;
    double smallRegionSize;
    double regionDistanceAverage;
    double regionDistanceVariance;
    double regionDistanceDeviation;


    public Set<Region> regionsIsolated = new HashSet<>();
    public Set<Region> regionsHighVar = new HashSet<>();
    public Set<Region> regionsElongated = new HashSet<>();
    public Set<Region> regionsBox = new HashSet<>();
    public Set<Region> regionsSmallDistant = new HashSet<>();

    private void findLetterCandidates(){
        Set<Region> discarded = new HashSet<>();
        for (Region region : regions) {
            // Remove isolated dots
            if (region.size() < 5) {
                discarded.add(region);
                regionsIsolated.add(region);
                continue;
            }

            // Remove regions with high stroke variance (like foliage)
            if (region.swtVariance > region.swtAverage*0.5){
                discarded.add(region);
                regionsHighVar.add(region);
                continue;
            }

            // Remove elongated regions with low stroke variance (thin lined)
            if (region.swtVariance < 0.1 && region.size()/region.swtAverage < 3){
                discarded.add(region);
                regionsElongated.add(region);
                continue;
            }
        }
        regions.removeAll(discarded);
        discarded.clear();

        regionSizeAverage = regions.stream().mapToDouble(x -> x.size()).sum() / regions.size();
        regionSizeVariance = regions.stream().mapToDouble(x -> pow(regionSizeAverage - x.size(),2)).sum() / regions.size();
        regionSizeDeviation = sqrt(regionSizeVariance);

        smallRegionSize = max(regionSizeAverage * 0.5, regionSizeAverage - regionSizeDeviation);

        // Remove regions whose bounding boxes contain more than 2 other regions (eg. frames)
        final List<Region> contained = new ArrayList<>();
        for (Region r1 : regions){
            Rect box = r1.boundingBox;
            for (Region r2 : regions){
                if (box.contains(r2.centroid))
                    contained.add(r2);
            }
            if (contained.size() > 2) {
                discarded.add(r1);
                regionsBox.add(r1);
            }
            contained.clear();
        }
        regions.removeAll(discarded);
        discarded.clear();

        // Find smallest distances between region centers
        Map<Region, Double> distance = new HashMap<>();
        Map<Region, Region> nearest = new HashMap<>();
        for (Region r1 : regions){
            for (Region r2 : regions) {
                if (r2.size() < smallRegionSize)
                    continue;

                double dist = r1.centroidDistance(r2);
                double distPrev = distance.getOrDefault(r1, Double.MAX_VALUE);

                if (dist > 0 && dist < distPrev) {
                    distance.put(r1, dist);
                    nearest.put(r1, r2);
                }
            }
        }
        regionDistanceAverage = distance.values().stream().mapToDouble(x -> x).average().orElse(0);
        regionDistanceVariance = distance.values().stream().mapToDouble(x -> pow(regionDistanceAverage - x, 2)).sum() / distance.size();
        regionDistanceDeviation = sqrt(regionDistanceVariance);

        double farDistance = min(regionDistanceAverage * 1.5, regionDistanceAverage + regionDistanceDeviation *2);

        for (Region region : regions){
            // Remove small regions with no immediate regular sized neighbours
            double dist = distance.getOrDefault(region, 0.0);
            if (region.size() < smallRegionSize && dist > farDistance){
                discarded.add(region);
                regionsSmallDistant.add(region);
            }
        }

        regions.removeAll(discarded);
    }

    private void findTextLines(){
        TextLineClustering clustering = new TextLineClustering();
        Set<TextLine> lines = clustering.Cluster(regions);
        this.lines.addAll(lines);
    }
}
