package cz.emphasis.textdetection.helpers;

import java.util.Iterator;
import java.util.List;
import java.util.stream.IntStream;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

public class StreamHelpers {

    public static <T> Stream<T> asStream(Iterator<T> it){
        return asStream(it, false);
    }

    private static <T> Stream<T> asStream(Iterator<T> it, boolean parallel) {
        Iterable<T> iterable = () -> it;
        return StreamSupport.stream(iterable.spliterator(), parallel);
    }

    public static <T> Stream<List<T>> slidingWindow(List<T> list, int size) {
        if (size > list.size())
            return Stream.empty();

        return IntStream.range(0, list.size() - size +1)
            .mapToObj(start -> list.subList(start, start + size));
    }

    public static <T> Stream<T> inDirection(List<T> list, boolean positive){
        return positive ? list.stream() : inReverse(list);
    }

    public static <T> Stream<T> inReverse(List<T> input) {
        return IntStream
            .range(1, input.size() + 1)
            .mapToObj(i -> input.get(input.size() - i));
    }
}
