package cz.emphasis.egt;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.PixelFormat;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.util.Pair;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.common.collect.ImmutableList;

import org.json.JSONException;
import org.json.JSONObject;
import org.opencv.core.Point;
import org.opencv.core.Rect;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import emphasis.egt.R;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Timed;

import static android.content.Context.BIND_AUTO_CREATE;

public class TestTextFinderFragment extends Fragment {

    private static final int REQUEST_MANAGE_OVERLAY_PERMISSION = 1;
    private static final String TAG = "TestTextFinderFragment";
    private static final long TEST_TEXT_SHUFFLE_INTERVAL = 40;
    private static final long TEXT_BOUNDING_BOX_SHOW_INTERVAL = 5000;

    private OnFragmentInteractionListener mListener;
    private Unbinder viewUnbinder;

    @BindView(R.id.textFinder_textViewFPS) public TextView mTextViewFPS;
    @BindView(R.id.textFinder_textViewMoving) public TextView mTextViewMoving;

    public TestTextFinderFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_test_text_finder, container, false);
        viewUnbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        viewUnbinder.unbind();
    }

    @OnClick(R.id.textFinder_container)
    public void toggleTextFinder(){

    }

    @Override
    public void onResume() {
        super.onResume();

        mWindowManager = (WindowManager) getContext().getSystemService(Context.WINDOW_SERVICE);

        checkDrawOverlaysPermission();

        initializeMovingText();
        startTextFinderService();
    }

    private void checkDrawOverlaysPermission(){
        if (!Settings.canDrawOverlays(getContext())) {
            Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION, Uri.parse("package:" + getActivity().getPackageName()));
            startActivityForResult(intent, REQUEST_MANAGE_OVERLAY_PERMISSION);
        }
    }

    @Override
    public void onPause() {
        super.onPause();

        stopMovingText();
        stopTextFinderService();

        clearTextBoundingBoxes();

        mWindowManager = null;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onDetach() {
        super.onDetach();

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_MANAGE_OVERLAY_PERMISSION) {
            if (Settings.canDrawOverlays(getContext())) {
                // You have permission
            }
        }
    }

    //region Services
    private ServiceConnection mTextFinderConnection = new ServiceConnection();
    private TextFinderService.Binder mTextFinderBinder;
    private Disposable mScreenshotSubscription;
    private Disposable mScreenTextSubscription;

    private void startTextFinderService(){
        Intent svc = new Intent(getActivity(), TextFinderService.class);
        getActivity().bindService(svc, mTextFinderConnection, BIND_AUTO_CREATE);
    }

    private void stopTextFinderService(){
        Log.d(TAG, "TextFinderService disconnecting from " + TAG + ".");

        getActivity().unbindService(mTextFinderConnection);
        mTextFinderBinder = null;

        if (mScreenshotSubscription != null){
            mScreenshotSubscription.dispose();
            mScreenshotSubscription = null;
        }

        if (mScreenTextSubscription != null){
            mScreenTextSubscription.dispose();
            mScreenTextSubscription = null;
        }
    }

    private class ServiceConnection implements android.content.ServiceConnection{

        @Override
        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            if (iBinder instanceof TextFinderService.Binder){
                mTextFinderBinder = (TextFinderService.Binder)iBinder;
                onTextFinderServiceConnected(mTextFinderBinder);
                Log.d(TAG, "TextFinderService bound to " + TAG + ".");
            }
        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {

        }
    }

    private WindowManager mWindowManager;
    private final Handler mScreenshotNotifyHandler = new Handler();
    private final Timer mScreenshotNotifyTimer = new Timer();
    private long mScreenshotTime1 = Long.MAX_VALUE;
    private long mScreenshotTime2 = 0;
    private long mScreenshotCount = 0;

    private final List<View> mOverlays = new ArrayList<>();

    private void onTextFinderServiceConnected(TextFinderService.Binder binder){
        TimerTask task = new TimerTask() {
            @Override
            public void run() {
            mScreenshotNotifyHandler.post(() -> {
                double sec = (mScreenshotTime2-mScreenshotTime1)*1E-3;
                final double count = sec/mScreenshotCount;
                DecimalFormat format = new DecimalFormat("0.00");
                if (mTextViewFPS != null)
                    mTextViewFPS.setText("FPS: " + format.format(count));
            });
            }
        };
        mScreenshotNotifyTimer.scheduleAtFixedRate(task, 0, 1000);

        mScreenshotSubscription = binder.getScreenShots()
            .forEach(x -> {
                mScreenshotCount++;
                long ms = TimeUnit.MILLISECONDS.convert(x.time(), x.unit());
                if (ms < mScreenshotTime1)
                    mScreenshotTime1 = ms;
                if (ms > mScreenshotTime2)
                    mScreenshotTime2 = ms;
            });

        mScreenTextSubscription = binder.getScreenText()
            .observeOn(AndroidSchedulers.mainThread())
            .forEach(this::showTextBoundingBoxes);
    }

    private void clearTextBoundingBoxes(){
        mOverlays.forEach(mWindowManager::removeView);
        mOverlays.clear();
    }

    private void showTextBoundingBoxes(Timed<Pair<List<Rect>, List<Point>>> result){
        clearTextBoundingBoxes();

        mScreenshotNotifyTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                clearTextBoundingBoxes();
            }
        }, TEXT_BOUNDING_BOX_SHOW_INTERVAL);

        List<Rect> boxes = result.value().first;
        List<Point> centers = result.value().second;

        WindowManager.LayoutParams topLeftParams = new WindowManager.LayoutParams(
            WindowManager.LayoutParams.WRAP_CONTENT,
            WindowManager.LayoutParams.WRAP_CONTENT,
            WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY,
            WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE | WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL,
            PixelFormat.TRANSLUCENT);
        topLeftParams.gravity = Gravity.LEFT | Gravity.TOP;

        for (Rect box : boxes){
            View overlay =  new View(getContext());

            topLeftParams.x = box.x;
            topLeftParams.y = box.y;
            topLeftParams.width = box.width;
            topLeftParams.height = box.height;

            //use a GradientDrawable with only one color set, to make it a solid color
            GradientDrawable border = new GradientDrawable();
            border.setColor(Color.TRANSPARENT);
            border.setStroke(2, Color.GREEN);
            overlay.setBackground(border);

            mOverlays.add(overlay);
            mWindowManager.addView(overlay, topLeftParams);
        }

        int i = 0;
        for (Point center : centers){
            View overlay =  new View(getContext());

            topLeftParams.x = (int)center.x;
            topLeftParams.y = (int)center.y;
            topLeftParams.width = 4;
            topLeftParams.height = 4;

            overlay.setBackgroundColor(0xFFFF0000);

            mOverlays.add(overlay);
            mWindowManager.addView(overlay, topLeftParams);
        }
    }
    //endregion
    //region Add a moving text on screen
    private Random mRandom = new Random();
    private List<View> mMovable;
    private Disposable mMovingSubscription;
    private RequestQueue mRequestQueue;

    private void requestNextPage(){
        if (mRequestQueue == null)
            return;

        // Pull a random page from wikipedia using Volley library
        final String url = "https://en.wikipedia.org/w/api.php?format=json&action=query&generator=random&prop=extracts&exintro=&explaintext=&grnnamespace=0&grnlimit=1";
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, null,
            response -> {
                try {
                    Log.d(TAG, "RECEIVED " + response.toString());
                    JSONObject query = response.getJSONObject("query");
                    JSONObject pages = query.getJSONObject("pages");
                    Iterator<String> pagesIt = pages.keys();
                    if (pagesIt.hasNext()){
                        String pageName = pagesIt.next();
                        JSONObject page = pages.getJSONObject(pageName);
                        String title = page.getString("title");
                        String extract = page.getString("extract");
                        if (extract.length() > 50 && extract.contains(".")){
                            Optional<String> line = Arrays.stream(extract.split("\\.")).filter(x -> x.length() > 10).findFirst();
                            if (line.isPresent()) {
                                StringBuilder sb = new StringBuilder();
                                sb.append(title);
                                sb.append(System.lineSeparator());
                                sb.append(line.get());
                                mTextViewMoving.setText(sb.toString());
                                Log.d(TAG,"Setting text to: " + sb.toString());
                                return;
                            }
                        }
                    }

                    requestNextPage();
                    return;
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            },
            error -> Log.e(TAG, error.toString()));

        mRequestQueue.add(request);
    }

    private void initializeMovingText() {
        mRequestQueue = Volley.newRequestQueue(getContext());
        requestNextPage();

        mMovable = ImmutableList.of(mTextViewMoving);

        mMovingSubscription = Observable.interval(TEST_TEXT_SHUFFLE_INTERVAL, TimeUnit.SECONDS)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(i ->{
                if (getView() == null)
                    return;

                requestNextPage();

                for(View item : mMovable){
                    int with = getView().getWidth();
                    int height = getView().getHeight();

                    int x = mRandom.nextInt((int)(with*0.3) - 50) + 50;
                    int y = mRandom.nextInt((int)(height*0.6) - 200) + 200;

                    move(item, x, y ,1.0f,1.0f);
                }
            });
    }

    private void move(View item, int x, int y, float xScale, float yScale){
        item.animate()
            .x(x).y(y)
            .scaleX(xScale).scaleY(yScale)
            .setDuration(1000);
    }

    private void stopMovingText(){
        if (mMovingSubscription != null){
            mMovingSubscription.dispose();
            mMovingSubscription = null;
        }
    }
    //endregion

    public interface OnFragmentInteractionListener {

    }
}
