package cz.emphasis.egt;

import android.app.Service;
import android.content.Intent;
import android.graphics.Rect;
import android.os.IBinder;

import kotlin.NotImplementedError;

public class EyeTrackingService extends Service {
    public EyeTrackingService() {
    }

    @Override
    public IBinder onBind(Intent intent) {
        return new Binder();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return super.onStartCommand(intent, flags, startId);
    }

    public class Binder extends android.os.Binder{

    }
}
