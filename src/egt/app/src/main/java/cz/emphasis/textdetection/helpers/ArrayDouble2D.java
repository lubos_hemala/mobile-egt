package cz.emphasis.textdetection.helpers;

import java.util.Arrays;

public class ArrayDouble2D extends Array2D {

    public final double[] source;

    public ArrayDouble2D(int width, int height){
        super(width, height);
        source = new double[width*height];
    }

    public final double get(DiscretePoint p){
        return get(p.x, p.y);
    }

    public final double get(int x, int y){
        return source[at(x,y)];
    }

    public final void put(DiscretePoint p, double value){
        put(p.x, p.y, value);
    }

    public final void put(int x, int y, double value){
        source[at(x,y)] = value;
    }

    public final void putIfLower(DiscretePoint p, double value){
        putIfLower(p.x, p.y, value);
    }

    public final void putIfLower(int x, int y, double value){
        int at = at(x,y);
        double current = source[at];
        if (value < current)
            source[at] = value;
    }

    public final boolean isZero(DiscretePoint p){
        return isZero(p.x, p.y);
    }

    public final boolean isZero(int x, int y){
        return get(x,y) == 0;
    }

    public final boolean isNonZero(DiscretePoint p){
        return isZero(p.x, p.y);
    }

    public final boolean isNonZero(int x, int y){
        return get(x,y) != 0;
    }

    public final void clear(){
        Arrays.fill(source, 0);
    }

    public final void fill(double value){
        Arrays.fill(source, value);
    }
}
