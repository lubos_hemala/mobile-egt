package cz.emphasis.textdetection.helpers;

import java.util.Arrays;

public class ArrayInteger2D extends Array2D {

    public final int[] source;

    public ArrayInteger2D(int width, int height){
        super(width, height);
        source = new int[width*height];
    }

    public final int get(DiscretePoint p){
        return get(p.x, p.y);
    }

    public final int get(int x, int y){
        return source[at(x,y)];
    }

    public final void put(DiscretePoint p, int value){
        put(p.x, p.y, value);
    }

    public final void put(int x, int y, int value){
        source[at(x,y)] = value;
    }

    public final void putIfLower(DiscretePoint p, int value){
        putIfLower(p.x, p.y, value);
    }

    public final void putIfLower(int x, int y, int value){
        int at = at(x,y);
        int current = source[at];
        if (value < current)
            source[at] = value;
    }

    public final boolean isZero(DiscretePoint p){
        return isZero(p.x, p.y);
    }

    public final boolean isZero(int x, int y){
        return get(x,y) == 0;
    }

    public final boolean isNonZero(DiscretePoint p){
        return isZero(p.x, p.y);
    }

    public final boolean isNonZero(int x, int y){
        return get(x,y) != 0;
    }

    public final void clear(){
        Arrays.fill(source, 0);
    }

    public final void fill(int value){
        Arrays.fill(source, value);
    }
}
