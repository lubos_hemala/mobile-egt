package cz.emphasis.textdetection.helpers;

import cz.emphasis.textdetection.Region;
import one.util.streamex.StreamEx;
import org.opencv.core.Point;
import org.opencv.core.Rect;

import java.util.List;

import static java.lang.Math.*;
import static java.lang.Math.pow;

public class Helpers {

    private static final double PI2 = PI*2;

    public static Point bottomCenter(Rect rect){
        return new Point(rect.x + rect.width/2, rect.y + rect.height);
    }

    public static Point rightCenter(Rect rect){
        return new Point(rect.x + rect.width, rect.y + rect.height/2);
    }

    public static Point leftCenter(Rect rect){
        return new Point(rect.x, rect.y + rect.height/2);
    }

    public static boolean isClose(double v1, double v2, double precision){
        double ratio = v1 > v2 ? v1/v2 : v2/v1;
        return ratio < precision;
    }

    public static boolean isOpposite(double alpha, double beta, double precision) {
        double diff = directionDiff(alpha, beta);
        return diff < precision;
    }

    public static double directionDiff(double alpha, double beta) {
        beta += PI;
        if (beta > PI2)
            beta -= PI2;
        double d1 = abs(alpha - beta) % PI2;
        double diff = Math.min(d1, abs(PI2 - d1));
        return diff;
    }

    public static double distance(DiscretePoint p1, DiscretePoint p2){
        return sqrt(distance2(p1,p2));
    }

    public static int distance2(DiscretePoint p1, DiscretePoint p2){
        int dx = p1.x - p2.x;
        int dy = p1.y - p2.y;
        int distance = dx*dx + dy*dy;
        return distance;
    }

    public static double distance(Point p1, Point p2){
        double distance = sqrt(distance2(p1,p2));
        return distance;
    }

    public static double distance2(Point p1, Point p2){
        double dx = p1.x - p2.x;
        double dy = p1.y - p2.y;
        double distance = dx*dx + dy*dy;
        return distance;
    }

    public static double direction(Point p1, Point p2){
        double dx = p1.x - p2.x;
        double dy = p1.y - p2.y;
        return direction(dy, dx);
    }

    public static double direction(double dy, double dx){
        return direction(dy, dx, false);
    }

    public static double direction(double dy, double dx, boolean opposite){
        double direction = atan2(dy, dx);

        if (opposite){
            direction += PI;
            if (direction > PI2) {
                direction -= PI2;
            }
        } else if (direction < 0) {
            direction += PI2;
        }

        return direction;
    }

    public static int compareDyDx(Point p1, Point p2){
        double dy = p1.y - p2.y;
        if (dy != 0)
            return (int)signum(dy);

        double dx = p1.x - p2.x;
        if (dy != 0)
            return (int)signum(dx);

        return 0;
    }

    public static double median(List<Double> values){
        if (values.size() == 0)
            return 0;

        values.sort(Double::compareTo);

        int size = values.size();
        int mid = (size-1)/2;
        if (size % 2 == 1)
            return values.get(mid);

        double m = (values.get(mid) + values.get(mid + 1))/2.0;
        return m;
    }

    public static double average(List<Double> values){
        if (values.size() == 0)
            return 0;

        double avg = 0;
        for (double v : values)
            avg += v;
        avg /= values.size();
        return avg;
    }

    public static double variance(double average, List<Double> values){
        if (values.size() == 0)
            return 0;

        double var = 0;
        for (double v : values) {
            double df = average - v;
            var += df*df;
        }
        var /= values.size();
        return var;
    }

    public static double averageHorizontalGap(List<Region> ln){
        boolean direction = ln.get(0).minX < ln.get(ln.size()-1).minX;
        double gap = StreamEx.of(StreamHelpers.inDirection(ln, direction))
            .pairMap((l,r) -> max(r.minX - l.maxX, 0))
            .mapToDouble(x -> x)
            .sum();
        if (ln.size() > 2)
            gap /= ln.size() -1;

        return gap;
    }
}
