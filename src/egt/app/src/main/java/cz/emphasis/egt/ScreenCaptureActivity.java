package cz.emphasis.egt;

import android.app.Activity;
import android.content.Intent;
import android.media.projection.MediaProjectionManager;
import android.os.ResultReceiver;
import android.os.Bundle;
import android.util.Log;

public class ScreenCaptureActivity extends Activity {

    private static final String TAG = "ScreenCaptureActivity";
    private static final int REQUEST_SCREENSHOT = 1000000;

    private MediaProjectionManager mMediaProjectionManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mMediaProjectionManager = (MediaProjectionManager)getSystemService(MEDIA_PROJECTION_SERVICE);

        startActivityForResult(mMediaProjectionManager.createScreenCaptureIntent(), REQUEST_SCREENSHOT);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent resultData) {
        if (requestCode != REQUEST_SCREENSHOT)
            return;

        if (resultCode != RESULT_OK) {
            Log.d(TAG, "Screen capture requested denied");
            finish();
        }

        ResultReceiver receiver = getIntent().getParcelableExtra(TextFinderService.SCREEN_CAPTURE_RECEIVER);

        Bundle result = new Bundle();
        result.putInt(TextFinderService.SCREEN_CAPTURE_RESULT_CODE, resultCode);
        result.putParcelable(TextFinderService.SCREEN_CAPTURE_RESULT_DATA, resultData);

        receiver.send(TextFinderService.RESULT_SCREEN_CAPTURE, result);

        finish();
    }
}
