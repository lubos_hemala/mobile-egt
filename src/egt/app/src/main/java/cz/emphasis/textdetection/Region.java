package cz.emphasis.textdetection;

import cz.emphasis.textdetection.helpers.DiscretePoint;
import cz.emphasis.textdetection.helpers.Helpers;
import org.opencv.core.Point;
import org.opencv.core.Rect;

import java.text.DecimalFormat;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import java.util.Set;

import static cz.emphasis.textdetection.helpers.Helpers.compareDyDx;
import static java.lang.Math.sqrt;

public class Region implements Iterable<DiscretePoint>, Comparable<Region> {
    public final int label;
    private final Set<DiscretePoint> points;

    public int size(){ return points.size(); }

    public double swtAverage;
    public double swtVariance;
    public double swtDeviation;
    public double swtMedian;
    public double centroidX;
    public double centroidY;
    public Point centroid;
    public int minX = Integer.MAX_VALUE;
    public int maxX;
    public int minY = Integer.MAX_VALUE;
    public int maxY;
    public Rect boundingBox;
    public int height;
    public int width;

    protected Region(int label, Set<DiscretePoint> points, List<Double> swt){
        this.label = label;
        this.points = points;

        initialize(points);
        initialize(swt);
    }

    public static Region create(int label, Set<DiscretePoint> points, List<Double> swt){
        return new Region(label, points, swt);
    }

    private final void initialize(List<Double> swt) {
        swtMedian = Helpers.median(swt);
        swtAverage = Helpers.average(swt);
        swtVariance = Helpers.variance(swtAverage, swt);
        swtDeviation = sqrt(swtVariance);
    }

    private final void initialize(Set<DiscretePoint> points){
        for (DiscretePoint p : points){
            if (p.x > maxX)
                maxX = p.x;
            else if (p.x < minX)
                minX = p.x;
            if (p.y > maxY)
                maxY = p.y;
            else if (p.y < minY)
                minY = p.y;

            centroidX += p.x;
            centroidY += p.y;
        }
        centroidX /= size();
        centroidY /= size();
        centroid = new Point(centroidX, centroidY);

        height = maxY - minY;
        width = maxX - minX;

        boundingBox = new Rect(minX, minY, width, height);
    }

    @Override
    public int compareTo(Region o) {
        return compareDyDx(centroid, o.centroid);
    }

    public double centroidDistance(Region other){
        return Helpers.distance(centroid, other.centroid);
    }

    public double centroidDirection(Region other){
        return Helpers.direction(centroid, other.centroid);
    }

    public double bottomCenterDirection(Region other){
        Point p1 = Helpers.bottomCenter(boundingBox);
        Point p2 = Helpers.bottomCenter(other.boundingBox);
        return Helpers.direction(p1, p2);
    }

    @Override
    public Iterator<DiscretePoint> iterator() {
        return points.iterator();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Region that = (Region) o;
        return label == that.label;
    }

    @Override
    public int hashCode() {
        return Objects.hash(label);
    }

    @Override
    public String toString() {
        DecimalFormat f = new DecimalFormat("0.00");
        return "@" + label + " " + DiscretePoint.round(centroid) + " size(" + size() + ") " + width + "x" + height + " |→| avg:" + f.format(swtAverage) + " var:" + f.format(swtVariance);
    }
}
