package cz.emphasis.egt;

import android.app.Service;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.PixelFormat;
import android.graphics.Point;
import android.media.Image;
import android.media.ImageReader;
import android.media.projection.MediaProjection;
import android.media.projection.MediaProjectionManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.ResultReceiver;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.Pair;
import android.view.Display;
import android.view.WindowManager;

import org.opencv.android.Utils;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.Rect;
import org.opencv.imgproc.Imgproc;

import java.nio.Buffer;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import cz.emphasis.textdetection.TextDetection;
import io.reactivex.Observable;
import io.reactivex.schedulers.Timed;
import io.reactivex.subjects.PublishSubject;
import io.reactivex.subjects.Subject;

import static android.hardware.display.DisplayManager.VIRTUAL_DISPLAY_FLAG_AUTO_MIRROR;

public class TextFinderService extends Service {

    private static final String TAG = "TextFinderService";
    private static final long TEXT_DETECTION_SAMPLING_INTERVAL = 10;

    public TextFinderService(){
    }

    @Override
    public void onCreate() {
        super.onCreate();

        requestScreenCapture();
        initializeTextFinding();
    }

    @Override
    public IBinder onBind(Intent intent) {
        return new Binder();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return super.onStartCommand(intent, flags, startId);
    }

    public class Binder extends android.os.Binder{
        public Observable<Timed<Mat>> getScreenShots(){
            return TextFinderService.this.getScreenShots();
        }

        public Observable<Timed<Pair<List<Rect>, List<org.opencv.core.Point>>>> getScreenText(){
            return TextFinderService.this.getScreenText();
        }
    }

    private Observable<Pair<List<Rect>, List<org.opencv.core.Point>>> mScreenText;

    public Observable<Timed<Pair<List<Rect>, List<org.opencv.core.Point>>>> getScreenText(){
        return mScreenText.timestamp();
    }

    private void initializeTextFinding(){
        if (mScreenText != null)
            return;


        Observable<Timed<Mat>> screenshots = getScreenShots();
        mScreenText =
            Observable.concat(
                screenshots.sample(2, TimeUnit.SECONDS).take(1),
                screenshots.sample(TEXT_DETECTION_SAMPLING_INTERVAL, TimeUnit.SECONDS))
            .map(x -> findText(x));
    }

    private final TextDetection textDetection = new TextDetection();
    private boolean isWorking = false;

    private Pair<List<Rect>, List<org.opencv.core.Point>> findText(Timed<Mat> next){
        Mat screenshot = next.value();

        Log.d(TAG, "Screenshot(" + next.time() + ") RECEIVED size: " + screenshot.size() + ", channels: " + screenshot.channels());

        double scale  = 1;
        if (!isWorking) {
            isWorking = true;
            long t1 = System.nanoTime();
            textDetection.findText(screenshot, scale);
            long t2 = System.nanoTime();
            long td = TimeUnit.NANOSECONDS.toMillis(t2-t1);
            Log.d(TAG, "Screenshot(" + next.time() + ") ELAPSED in " + td + "ms");
        }
        isWorking = false;

        List<org.opencv.core.Point> centers = textDetection.regions.stream().limit(0).map(region -> {
            int x = (int)(region.centroid.x/scale);
            int y = (int)(region.centroid.y/scale);
            return new org.opencv.core.Point(x,y);
        }).collect(Collectors.toList());

        List<Rect> boxes = textDetection.lines.stream().map(line -> {
            Rect box = line.boundingBox();
            int x = (int)(box.x/scale);
            int y = (int)(box.y/scale);
            int w = (int)(box.width/scale);
            int h = (int)(box.height/scale);
            return new Rect(x,y,w,h);
        }).collect(Collectors.toList());

        if (boxes.isEmpty())
            Log.d(TAG, "Screenshot(" + next.time() + ") has no lines");
        for (Rect box : boxes)
            Log.d(TAG, "Screenshot(" + next.time() + ") line " + box);

        return new Pair<>(boxes, centers);
    }

    //region Screen capture permission initialization
    public static final String SCREEN_CAPTURE_RECEIVER = "SCREEN_CAPTURE_RECEIVER";
    public static final String SCREEN_CAPTURE_RESULT_CODE = "SCREEN_CAPTURE_RESULT_CODE";
    public static final String SCREEN_CAPTURE_RESULT_DATA = "SCREEN_CAPTURE_RESULT_DATA";
    public static final int RESULT_SCREEN_CAPTURE = 2000000;

    private int mScreenCaptureResultCode;
    private Intent mScreenCaptureResultData;

    private boolean requestScreenCapture(){
        if (mScreenCaptureResultData != null)
            return true;

        Intent intent = new Intent(this, ScreenCaptureActivity.class);
        intent.putExtra(SCREEN_CAPTURE_RECEIVER, new ScreenCaptureMessageReceiver());

        startActivity(intent);

        return false;
    }

    public class ScreenCaptureMessageReceiver extends ResultReceiver {

        public ScreenCaptureMessageReceiver() {
            super(null);
        }

        @Override
        protected void onReceiveResult(int resultCode, Bundle resultData) {
            if (resultCode != RESULT_SCREEN_CAPTURE)
                return;

            mScreenCaptureResultCode = resultData.getInt(SCREEN_CAPTURE_RESULT_CODE);
            mScreenCaptureResultData = resultData.getParcelable(SCREEN_CAPTURE_RESULT_DATA);

            startScreenCapture();
        }
    }
    //endregion
    //region Screen capture
    private final String SCREEN_CAPTURE_DISPLAY = "SCREEN_CAPTURE_DISPLAY";
    private final Object mScreenCaptureLock = new Object();
    private MediaProjection mMediaProjection;
    private ImageReader mImageReader;
    private int mScreenWidth;
    private int mScreenHeight;
    private HandlerThread mThread;
    private Handler mBackgroundHandler;
    private boolean isScreenCaptured;

    private final Subject<Mat> mScreenShots = PublishSubject.create();

    public io.reactivex.Observable<Timed<Mat>> getScreenShots(){
        return mScreenShots.timestamp();
    }

    private void startScreenCapture(){
        synchronized (mScreenCaptureLock) {
            if (isScreenCaptured)
                return;

            isScreenCaptured = true;
        }

        // Retrieve display metrics
        DisplayMetrics metrics = new DisplayMetrics();
        WindowManager windowManager = (WindowManager) getSystemService(WINDOW_SERVICE);
        Display display = windowManager.getDefaultDisplay();
        display.getMetrics(metrics);
        Point size = new Point();
        display.getSize(size);
        mScreenWidth = size.x;
        mScreenHeight = size.y;
        int dpi = metrics.densityDpi;

        // Prepare thread for screen capture
        mThread = new HandlerThread("TextFinder");
        mThread.start();
        mBackgroundHandler = new Handler(mThread.getLooper());

        // Start capturing screen
        MediaProjectionManager projectionManager = (MediaProjectionManager) getSystemService(MEDIA_PROJECTION_SERVICE);
        mMediaProjection = projectionManager.getMediaProjection(mScreenCaptureResultCode, mScreenCaptureResultData);
        mImageReader = ImageReader.newInstance(mScreenWidth, mScreenHeight, PixelFormat.RGBA_8888, 2);
        mMediaProjection.createVirtualDisplay(SCREEN_CAPTURE_DISPLAY, mScreenWidth, mScreenHeight, dpi, VIRTUAL_DISPLAY_FLAG_AUTO_MIRROR, mImageReader.getSurface(), null, mBackgroundHandler);
        mImageReader.setOnImageAvailableListener(new ScreenImageAvailableListener(), mBackgroundHandler);
    }

    private class ScreenImageAvailableListener implements ImageReader.OnImageAvailableListener {

        @Override
        public void onImageAvailable(ImageReader reader) {
            try (Image image = mImageReader.acquireLatestImage()) {
                if (image == null)
                    return;

                Image.Plane[] planes = image.getPlanes();
                Buffer imageBuffer = planes[0].getBuffer().rewind();
                int pixelStride = planes[0].getPixelStride();
                int rowStride = planes[0].getRowStride();
                int rowPadding = rowStride - pixelStride * mScreenWidth;
                int bitmapWidth = mScreenWidth + rowPadding / pixelStride;

                Bitmap bitmap = Bitmap.createBitmap(bitmapWidth, mScreenHeight, Bitmap.Config.ARGB_8888);
                bitmap.copyPixelsFromBuffer(imageBuffer);

                Mat mat = new Mat();
                Utils.bitmapToMat(bitmap, mat);

                mScreenShots.onNext(mat);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
    //endregion

}
