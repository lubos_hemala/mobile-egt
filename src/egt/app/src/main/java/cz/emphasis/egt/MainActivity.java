package cz.emphasis.egt;

import android.content.ComponentName;
import android.content.Intent;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import org.opencv.android.OpenCVLoader;

import emphasis.egt.R;

public class MainActivity extends AppCompatActivity implements MainFragment.OnFragmentInteractionListener {

    private static final String TAG = "MainActivity";

    static {
        if (!OpenCVLoader.initDebug())
            Log.e(TAG, "Unable to load OpenCV.");
        else
            Log.d(TAG, "OpenCV loaded.");
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (savedInstanceState == null) {
            setContent(new MainFragment(), false);
        }

        startTextFinderService();
        startEyeTrackerService();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (mEyeTrackerConnection != null) {
            unbindService(mEyeTrackerConnection);
            mEyeTrackerConnection = null;
        }
        if (mTextFinderConnection != null){
            unbindService(mTextFinderConnection);
            mTextFinderConnection = null;
        }
    }

    //region Start services
    private ServiceConnection mTextFinderConnection;
    private ServiceConnection mEyeTrackerConnection;

    private TextFinderService.Binder mTextFinderServiceBinder;
    private EyeTrackingService.Binder mEyeTrackingServiceBinder;

    private void startTextFinderService(){
        Intent svc = new Intent(this, TextFinderService.class);
        startService(svc);

        mTextFinderConnection = new ServiceConnection();
        bindService(svc, mTextFinderConnection, BIND_AUTO_CREATE);
    }

    private void startEyeTrackerService() {
        Intent svc = new Intent(this, EyeTrackingService.class);
        startService(svc);

        mEyeTrackerConnection = new ServiceConnection();
        bindService(svc, mEyeTrackerConnection, BIND_AUTO_CREATE);
    }

    private class ServiceConnection implements android.content.ServiceConnection{

        @Override
        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            if (iBinder instanceof TextFinderService.Binder){
                mTextFinderServiceBinder = (TextFinderService.Binder)iBinder;
                Log.d(TAG, "TextFinderService bound to " + TAG + ".");
            } else if (iBinder instanceof EyeTrackingService.Binder){
                mEyeTrackingServiceBinder = (EyeTrackingService.Binder)iBinder;
                Log.d(TAG, "EyeTrackerService bound to " + TAG + ".");
            }
        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {

        }
    }
    //endregion

    private void setContent(Fragment content, boolean allowBackStack) {
        FragmentTransaction trans =
            getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.mainLayout, content);
        if (allowBackStack)
            trans.addToBackStack(null);
        trans.commit();
    }

    @Override
    public void testTextFinder(){
        setContent(new TestTextFinderFragment(), true);
    }

    @Override
    public void testEyeTracker(){
        setContent(new TestEyeTrackerFragment(), true);
    }

}
