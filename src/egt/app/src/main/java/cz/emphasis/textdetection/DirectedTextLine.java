package cz.emphasis.textdetection;

import com.google.common.base.Supplier;
import com.google.common.base.Suppliers;
import cz.emphasis.textdetection.helpers.Helpers;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class DirectedTextLine{
    public final TextLine line;
    public final Region start;
    public final boolean rotate;

    public DirectedTextLine(TextLine line, Region start){
        this.line = line;
        this.start = start;
        this.rotate = !start.equals(line.getFirst());
    }

    public Region getFirst(){ return !rotate ? line.getFirst() : line.getLast(); }
    public Region getSecond(){ return !rotate ? line.getSecond() : getSecondLast(); }
    public Region getLast(){ return !rotate ? line.getLast() : line.getFirst(); }
    public Region getSecondLast(){ return !rotate ? line.getSecond() : line.getSecondLast(); }

    public final Supplier<Double> centroidDirection = Suppliers.memoize(() -> getFirst().centroidDirection(getLast()));
    public final Supplier<Double> bottomCenterDirection = Suppliers.memoize(() -> getFirst().bottomCenterDirection(getLast()));

    public final Supplier<Double> centroidDirection1st2nd = Suppliers.memoize(() -> getFirst().centroidDirection(getSecond()));
    public final Supplier<Double> bottomCenterDirection1st2nd = Suppliers.memoize(() -> getFirst().bottomCenterDirection(getSecond()));

    public boolean isOpposite(DirectedTextLine other, double precision){
        return
            Helpers.isOpposite(centroidDirection.get(), other.centroidDirection.get(), precision) ||
            Helpers.isOpposite(bottomCenterDirection.get(), other.bottomCenterDirection.get(), precision);
    }

    public boolean isOpposite1st2nd(DirectedTextLine other, double precision){
        return
            Helpers.isOpposite(centroidDirection1st2nd.get(), other.centroidDirection1st2nd.get(), precision) ||
            Helpers.isOpposite(bottomCenterDirection1st2nd.get(), other.bottomCenterDirection1st2nd.get(), precision);
    }

    public boolean isOpposite(TextLine other, double precision) {
        return
            other.isCentroidOpposite(centroidDirection.get(), precision) ||
            other.isBottomCenterOpposite(bottomCenterDirection.get(), precision);
    }

    public double centroidDistance1st2nd(){
        Region r1 = getFirst();
        Region r2 = getSecond();
        double distance = r1.centroidDistance(r2);
        return distance;
    }

    public double averageHorizontalGap(){
        return Helpers.averageHorizontalGap(line.line);
    }

    public double horizontalGap1st2nd(){
        return Helpers.averageHorizontalGap(Arrays.asList(getFirst(), getSecond()));
    }

    public double verticalGap1st2nd(){
        Region r1 = getFirst();
        Region r2 = getSecond();
        boolean direction = r1.minY < r2.minY;
        double gap = direction ? (r2.minY - r1.maxY) : (r1.minY - r2.maxX);
        return gap;
    }

    public double averageWidth(){
        return line.line.stream().mapToDouble(x -> x.width).average().getAsDouble();
    }

    public TextLine connect(DirectedTextLine d2){
        boolean r1 = rotate, r2 = d2.rotate;
        List<Region> l1, l2;
        if (r1){
            l1 = getSourceLine();
            l2 = r2 ? d2.getReverseLine() : d2.getSourceLine();
        } else {
            l1 = r2 ? d2.getSourceLine() : d2.getReverseLine();
            l2 = getSourceLine();
        }
        l2.remove(0);
        l1.addAll(l2);
        return new TextLine(l1);
    }

    public List<Region> getReverseLine(){
        List<Region> result = getSourceLine();
        Collections.reverse(result);
        return result;
    }

    public List<Region> getSourceLine(){
        List<Region> result = new ArrayList<>(line.line);
        return result;
    }

    public static DirectedTextLine from(Region start, Region end){
        return new DirectedTextLine(new TextLine(start, end), start);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DirectedTextLine that = (DirectedTextLine) o;
        return line.equals(that.line);
    }

    @Override
    public int hashCode() {
        return line.hashCode();
    }

    @Override
    public String toString() {
        return rotate ? line + "@" : "@" + line;
    }
}